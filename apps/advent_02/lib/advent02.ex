defmodule Advent02 do
  @moduledoc """
  Source code for Advent of Code puzzle (Day 2).
  """

  @doc """
  Computes the instructions coded in a list-memory.

  ## Examples

      iex> Advent02.compute([1,0,0,0,99])
      [2,0,0,0,99]

      iex> Advent02.compute([2,3,0,3,99])
      [2,3,0,6,99]

      iex> Advent02.compute([2,4,4,5,99,0])
      [2,4,4,5,99,9801]

      iex> Advent02.compute([1,1,1,4,99,5,6,0,99])
      [30,1,1,4,2,5,6,0,99]

      iex> Advent02.compute([1,9,10,3,2,3,11,0,99,30,40,50])
      [3500,9,10,70,2,3,11,0,99,30,40,50]
      
  """
  @spec compute(list(integer())) :: list(integer())
  def compute(program) do
    compute(program, program, 0)
  end

  @spec compute(list(integer()), list(integer()), integer()) :: list(integer())
  defp compute([99 | _rest], memory, _) do
    memory
  end

  defp compute([opcode, pos_arg1, pos_arg2, pos_result | _rest], memory, num_ops) do
    result = execute(opcode, Enum.at(memory, pos_arg1), Enum.at(memory, pos_arg2))
    new_memory = List.replace_at(memory, pos_result, result)
    new_rest = Enum.slice(new_memory, ((num_ops + 1) * 4)..length(new_memory))
    compute(new_rest, new_memory, num_ops + 1)
  end

  @spec execute(integer(), integer(), integer()) :: integer()
  defp execute(1, arg1, arg2) do
    arg1 + arg2
  end

  defp execute(2, arg1, arg2) do
    arg1 * arg2
  end

  @doc """
  Reads program-memory from file and computes it.
  Takes a noun and verb as initial arguments for positions 1 and 2 of the memory.
  """
  @spec load_and_compute_program(String.t(), integer(), integer()) :: list(integer())
  def load_and_compute_program(filename, noun, verb) do
    memory = load(filename)
    compute_program(memory, noun, verb)
  end

  @spec compute_program(list(integer()), integer(), integer()) :: list(integer())
  defp compute_program(memory, noun, verb) do
    # as instructed!
    new_memory = List.replace_at(memory, 1, noun) |> List.replace_at(2, verb)
    compute(new_memory)
  end

  @spec load(String.t()) :: list(integer())
  defp load(filename) do
    Path.expand(filename)
    |> Path.absname()
    |> File.read!()
    |> String.trim("\n")
    |> String.split(",")
    |> Enum.map(&String.to_integer(&1))
    |> Enum.to_list()
  end

  @doc """
  Given a list-memory locates instruction arguments A (noun) and B (verb)
  which cause the program to yield a specific provided value as result on position 0.
  """
  @spec locate_inputs(String.t(), integer()) :: {integer(), integer()}
  def locate_inputs(filename, value) do
    memory = load(filename)
    create_processes(memory, 99, 99, value, self())

    receive do
      {:found_value, noun, verb} -> {noun, verb}
    end
  end

  @spec create_processes(list(integer()), integer(), integer(), integer(), pid()) :: {:ok, pid()}
  defp create_processes(memory, 0, 0, value, parent) do
    Task.start(fn -> find_value(memory, 0, 0, value, parent) end)
  end

  defp create_processes(memory, 0, verb, value, parent) do
    Task.start(fn -> find_value(memory, 0, verb, value, parent) end)
    create_processes(memory, 99, verb - 1, value, parent)
  end

  defp create_processes(memory, noun, verb, value, parent) do
    Task.start(fn -> find_value(memory, noun, verb, value, parent) end)
    create_processes(memory, noun - 1, verb, value, parent)
  end

  @spec find_value(list(integer()), integer(), integer(), integer(), pid()) ::
          {:found_value, integer(), integer()} | :ok
  defp find_value(memory, noun, verb, value, parent) do
    reply_if_found(noun, verb, compute_program(memory, noun, verb) |> List.first(), value, parent)
  end

  @spec reply_if_found(integer(), integer(), integer(), integer(), pid()) ::
          {:found_value, integer(), integer()} | :ok
  defp reply_if_found(noun, verb, value, value, parent) do
    send(parent, {:found_value, noun, verb})
  end

  defp reply_if_found(_noun, _verb, _anothervalue, _value, _parent) do
    :ok
  end
end
