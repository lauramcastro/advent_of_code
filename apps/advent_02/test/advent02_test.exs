defmodule Advent02Test do
  use ExUnit.Case
  doctest Advent02

  test "passes the first sub-challenge" do
    assert Advent02.load_and_compute_program("priv/input.txt", 12, 2) |> List.first() == 4_484_226
  end

  test "the first sub-challenge can be used to test the second one" do
    assert Advent02.locate_inputs("priv/input.txt", 4_484_226) == {12, 2}
  end

  test "passes the second sub-challenge" do
    assert Advent02.locate_inputs("priv/input.txt", 19_690_720) == {56, 96}
  end
end
