defmodule Advent05 do
  @moduledoc """
  Source code for Advent of Code puzzle (Day 5).
  """

  @position_mode 0
  @immediate_mode 1

  @doc """
  Computes the instructions coded in a list-memory.

      iex> Advent05.compute([1002,4,3,4,33])
      [1002,4,3,4,99]
      
      iex> Advent05.compute([1101,100,-1,4,0])
      [1101,100,-1,4,99]
  """
  @spec compute(IO.device(), list(integer())) :: list(integer())
  def compute(iodevice \\ :stdio, program) do
    compute_aux(
      iodevice,
      hd(program) |> Integer.digits() |> add_leading_zeroes,
      tl(program),
      program,
      0
    )
  end

  @spec compute_aux(IO.device(), list(integer()), list(integer()), list(integer()), integer) ::
          list(integer())
  defp compute_aux(_iodevice, [0, 0, 0, 9, 9], _rest, memory, _offset), do: memory

  defp compute_aux(iodevice, [0, 0, 0, 0, 3], [pos | _rest], memory, offset) do
    input = IO.gets(iodevice, "Input? ") |> String.trim("\n") |> String.to_integer()
    {new_memory, new_rest} = update(pos, input, memory, offset + 2)

    compute_aux(
      iodevice,
      hd(new_rest) |> Integer.digits() |> add_leading_zeroes,
      tl(new_rest),
      new_memory,
      offset + 2
    )
  end

  defp compute_aux(iodevice, [0, 0, mode, 0, 4], [pos | rest], memory, offset) do
    value = retrieve(mode, pos, memory)
    IO.puts(iodevice, "Output: #{value}")

    compute_aux(
      iodevice,
      hd(rest) |> Integer.digits() |> add_leading_zeroes,
      tl(rest),
      memory,
      offset + 2
    )
  end

  defp compute_aux(iodevice, [0, mode_b, mode_a, 0, 5], [a, b | rest], memory, offset) do
    case retrieve(mode_a, a, memory) do
      0 ->
        compute_aux(
          iodevice,
          hd(rest) |> Integer.digits() |> add_leading_zeroes,
          tl(rest),
          memory,
          offset + 3
        )

      _N ->
        value_b = retrieve(mode_b, b, memory)
        new_rest = Enum.slice(memory, value_b..length(memory))

        compute_aux(
          iodevice,
          hd(new_rest) |> Integer.digits() |> add_leading_zeroes,
          tl(new_rest),
          memory,
          value_b
        )
    end
  end

  defp compute_aux(iodevice, [0, mode_b, mode_a, 0, 6], [a, b | rest], memory, offset) do
    case retrieve(mode_a, a, memory) do
      0 ->
        value_b = retrieve(mode_b, b, memory)
        new_rest = Enum.slice(memory, value_b..length(memory))

        compute_aux(
          iodevice,
          hd(new_rest) |> Integer.digits() |> add_leading_zeroes,
          tl(new_rest),
          memory,
          value_b
        )

      _N ->
        compute_aux(
          iodevice,
          hd(rest) |> Integer.digits() |> add_leading_zeroes,
          tl(rest),
          memory,
          offset + 3
        )
    end
  end

  defp compute_aux(iodevice, [0, mode_b, mode_a, 0, op], [a, b, c | _rest], memory, offset) do
    value_a = retrieve(mode_a, a, memory)
    value_b = retrieve(mode_b, b, memory)
    result = execute(op, value_a, value_b)
    {new_memory, new_rest} = update(c, result, memory, offset + 4)

    compute_aux(
      iodevice,
      hd(new_rest) |> Integer.digits() |> add_leading_zeroes,
      tl(new_rest),
      new_memory,
      offset + 4
    )
  end

  @spec add_leading_zeroes(list(integer())) :: list(integer())
  defp add_leading_zeroes(list) when length(list) == 5, do: list
  defp add_leading_zeroes(list), do: add_leading_zeroes([0 | list])

  @spec retrieve(integer(), integer(), list(integer())) :: integer()
  defp retrieve(@immediate_mode, value, _memory), do: value
  defp retrieve(@position_mode, position, memory), do: Enum.at(memory, position)

  @spec update(integer(), integer(), list(integer()), integer()) ::
          {list(integer()), list(integer())}
  defp update(position, value, memory, offset) do
    new_memory = List.replace_at(memory, position, value)
    new_rest = Enum.slice(new_memory, offset..length(new_memory))
    {new_memory, new_rest}
  end

  @spec execute(integer, integer, integer) :: integer
  defp execute(1, arg1, arg2) do
    arg1 + arg2
  end

  defp execute(2, arg1, arg2) do
    arg1 * arg2
  end

  defp execute(8, arg, arg), do: 1
  defp execute(8, _arg1, _arg2), do: 0

  defp execute(7, arg1, arg2) when arg1 < arg2, do: 1
  defp execute(7, _arg1, _arg2), do: 0

  @doc """
  Reads program-memory from file and computes it.
  """
  @spec load_and_compute_program(String.t()) :: list(integer())
  def load_and_compute_program(filename) do
    memory =
      Path.expand(filename)
      |> Path.absname()
      |> File.read!()
      |> String.trim("\n")
      |> String.split(",")
      |> Enum.map(&String.to_integer(&1))
      |> Enum.to_list()

    compute(memory)
  end
end
