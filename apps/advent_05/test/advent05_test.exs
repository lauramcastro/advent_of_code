defmodule Advent05Test do
  use ExUnit.Case
  doctest Advent05
  import ExUnit.CaptureIO

  test "passes the first sub-challenge" do
    assert capture_io(
             [input: "1"],
             fn -> Advent05.load_and_compute_program("priv/input.txt") end
           ) ==
             "Input? Output: 0\n" <>
               "Output: 0\n" <>
               "Output: 0\n" <>
               "Output: 0\n" <>
               "Output: 0\n" <>
               "Output: 0\n" <>
               "Output: 0\n" <>
               "Output: 0\n" <>
               "Output: 0\n" <>
               "Output: 5182797\n"
  end

  test "outputs 1 if input equal to 8 (position mode)" do
    assert capture_io(
             [input: "8"],
             fn -> Advent05.compute([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]) end
           ) ==
             "Input? Output: 1\n"
  end

  test "outputs 0 if input not equal to 8 (position mode)" do
    assert capture_io(
             [input: "0"],
             fn -> Advent05.compute([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]) end
           ) ==
             "Input? Output: 0\n"
  end

  test "outputs 1 if input less than 8 (position mode)" do
    assert capture_io(
             [input: "7"],
             fn -> Advent05.compute([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]) end
           ) ==
             "Input? Output: 1\n"
  end

  test "outputs 0 if input not less than 8 (position mode)" do
    assert capture_io(
             [input: "8"],
             fn -> Advent05.compute([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]) end
           ) ==
             "Input? Output: 0\n"
  end

  test "outputs 1 if input equal to 8 (immediate mode)" do
    assert capture_io(
             [input: "8"],
             fn -> Advent05.compute([3, 3, 1108, -1, 8, 3, 4, 3, 99]) end
           ) ==
             "Input? Output: 1\n"
  end

  test "outputs 0 if input not equal to 8 (immediate mode)" do
    assert capture_io(
             [input: "0"],
             fn -> Advent05.compute([3, 3, 1108, -1, 8, 3, 4, 3, 99]) end
           ) ==
             "Input? Output: 0\n"
  end

  test "outputs 1 if input less than 8 (immediate mode)" do
    assert capture_io(
             [input: "7"],
             fn -> Advent05.compute([3, 3, 1107, -1, 8, 3, 4, 3, 99]) end
           ) ==
             "Input? Output: 1\n"
  end

  test "outputs 0 if input not less than 8 (immediate mode)" do
    assert capture_io(
             [input: "8"],
             fn -> Advent05.compute([3, 3, 1107, -1, 8, 3, 4, 3, 99]) end
           ) ==
             "Input? Output: 0\n"
  end

  test "outputs 0 if input is 0 (position mode)" do
    assert capture_io(
             [input: "0"],
             fn -> Advent05.compute([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9]) end
           ) ==
             "Input? Output: 0\n"
  end

  test "outputs 1 if input is not 0 (position mode)" do
    assert capture_io(
             [input: "-1"],
             fn -> Advent05.compute([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9]) end
           ) ==
             "Input? Output: 1\n"
  end

  test "outputs 0 if input is 0 (immediate mode)" do
    assert capture_io(
             [input: "0"],
             fn -> Advent05.compute([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1]) end
           ) ==
             "Input? Output: 0\n"
  end

  test "outputs 1 if input is not 0 (immediate mode)" do
    assert capture_io(
             [input: "-1"],
             fn -> Advent05.compute([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1]) end
           ) ==
             "Input? Output: 1\n"
  end

  test "outputs 999 if input is below 8" do
    assert capture_io(
             [input: "7"],
             fn ->
               Advent05.compute([
                 3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106,
                 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105,
                 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99 
               ])
             end
           ) ==
             "Input? Output: 999\n"
  end

  test "outputs 1000 if input is 8" do
    assert capture_io(
             [input: "8"],
             fn ->
               Advent05.compute([
                 3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106,
                 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105,
                 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99
               ])
             end
           ) ==
             "Input? Output: 1000\n"
  end

  test "outputs 1001 if input is greater than 8" do
    assert capture_io(
             [input: "9"],
             fn ->
              Advent05.compute([
                3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106,
                0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105,
                1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99
              ])
             end
           ) ==
             "Input? Output: 1001\n"
  end

  test "passes the second sub-challenge" do
    assert capture_io(
             [input: "5"],
             fn -> Advent05.load_and_compute_program("priv/input.txt") end
           ) ==
             "Input? Output: 12077198\n"
  end
end
