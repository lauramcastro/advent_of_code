defmodule Advent07Test do
  use ExUnit.Case
  doctest Advent07

  test "passes the first sub-challenge" do
    assert Advent07.find_highest_signal("priv/input.txt") == {34_686, [4, 2, 1, 0, 3]}
  end
end
