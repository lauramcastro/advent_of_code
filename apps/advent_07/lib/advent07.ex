defmodule Advent07 do
  @moduledoc """
  Source code for Advent of Code puzzle (Day 7).
  """

  @doc """
  Calculates the thruster signal value produced by a given program under certain settings.

  ## Examples

      iex> Advent07.max_thruster([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0], [4,3,2,1,0])
      43210

      iex> Advent07.max_thruster([3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0], [0,1,2,3,4])
      54321

      iex> Advent07.max_thruster([3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0], [1,0,4,3,2])
      65210

  """
  @spec max_thruster(list(integer()), list(integer())) :: integer()
  def max_thruster(program, settings) do
    # s1, s2, s3, s4, and s5 are ALL DIFERENT
    run_circuit(0, program, settings)
  end

  @spec run_circuit(integer(), list(integer()), list(integer())) :: integer()
  defp run_circuit(input, program, [s1, s2, s3, s4, s5]) do
    compute(to_string(input), program, s1)
    |> compute(program, s2)
    |> compute(program, s3)
    |> compute(program, s4)
    |> compute(program, s5)
    |> String.to_integer()
  end

  @spec compute(String.t(), list(integer()), integer()) :: String.t()
  defp compute(input, program, setting) do
    {:ok, iodevice} = StringIO.open(to_string(setting) <> "\n" <> input <> "\n")
    Advent05.compute(iodevice, program)
    {:ok, {"", output}} = StringIO.close(iodevice)
    ["Output:", output_value] = String.trim(output, "\n") |> String.split()
    output_value
  end

  @doc """
  Figures out the settings for the highest thruster signal value produced by a program stored in a file.

  """
  @spec find_highest_signal(String.t()) :: {integer(), list(integer())}
  def find_highest_signal(filename) do
    program = load_program(filename)
    # can't do this one with tasks since they will be clashing on devices' names!
    permutations([0, 1, 2, 3, 4])
    |> Enum.map(fn settings -> {max_thruster(program, settings), settings} end)
    |> Enum.sort()
    |> List.last()
  end

  @spec permutations(list(integer())) :: list(list(integer()))
  defp permutations([]), do: [[]]
  defp permutations(list), do: for(h <- list, t <- permutations(list -- [h]), do: [h | t])

  @spec load_program(String.t()) :: list(integer())
  defp load_program(filename) do
    Path.expand(filename)
    |> Path.absname()
    |> File.read!()
    |> String.trim("\n")
    |> String.split(",")
    |> Enum.map(&String.to_integer(&1))
    |> Enum.to_list()
  end

  # @doc """
  # Calculates the thruster signal value produced by a given program under certain settings (with feedback loop).

  # ## Examples

  #     iex> Advent07.max_thruster_loop([3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5], [9,8,7,6,5])
  #     139629729

  #     iex> Advent07.max_thruster_loop([3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,
  #     .. 54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10], [9,7,8,5,6])
  #     18216

  # """
  # @spec max_thruster_loop(program :: list(integer), settings :: list(integer)) :: integer
  # def max_thruster_loop(program, settings) do
  #   agents =
  #     Enum.map(
  #       settings,
  #       fn s ->
  #         {:ok, pid} = Agent.start_link(fn -> {program, s} end)
  #         pid
  #       end
  #     )

  #   compute_loop("0", agents, false)
  # end

  # defp compute_loop(input, [agent | agents], false) do
  #   {program, setting} = Agent.get(agent, fn state -> state end)
  #   {:ok, iodevice} = StringIO.open(to_string(setting) <> "\n" <> input <> "\n")
  #   new_program = Advent05.compute(iodevice, program)
  #   Agent.update(agent, fn _ -> new_program end)
  #   {:ok, {"", output}} = StringIO.close(iodevice)
  #   ["Output:", output_value] = String.trim(output, "\n") |> String.split()
  #   compute_loop(output_value, agents ++ [agent], false)
  # end

  # defp compute_loop(output, agents, true) do
  #   Enum.map(agents, fn pid -> :ok = Agent.stop(pid) end)
  #   output
  # end
end
