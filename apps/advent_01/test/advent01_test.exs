defmodule Advent01Test do
  use ExUnit.Case
  doctest Advent01

  test "passes the first challenge" do
    assert Advent01.fuel_sum("priv/input.txt") == 3_239_890
  end

  test "passes the second challenge" do
    assert Advent01.fuel_sum("priv/input.txt", true) == 4_856_963
  end
end
