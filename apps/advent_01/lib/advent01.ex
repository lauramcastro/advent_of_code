defmodule Advent01 do
  @moduledoc """
  Source code for Advent of Code puzzle (Day 1).
  """

  @doc """
  Calculates required fuel for a spacecraft module, based on its mass.

  ## Examples

      iex> Advent01.required_fuel(12)
      2

      iex> Advent01.required_fuel(14)
      2

      iex> Advent01.required_fuel(1969)
      654

      iex> Advent01.required_fuel(100756)
      33583

      iex> Advent01.required_fuel(14, true)
      2

      iex> Advent01.required_fuel(1969, true)
      966

      iex> Advent01.required_fuel(100756, true)
      50_346

  """
  @spec required_fuel(integer()) :: integer()
  def required_fuel(mass, fuel_for_fuel \\ false)
  def required_fuel(mass, false) do
    Integer.floor_div(mass, 3) - 2
  end
  def required_fuel(mass, true) do
    required_fuel_for_fuel(mass, 0)
  end

  defp required_fuel_for_fuel(fuel, acc) when fuel <= 0 do
    # we undo the last incorrent addition in case it was negative
    acc + fuel * (-1)
  end

  defp required_fuel_for_fuel(fuel, acc) do
    morefuel = required_fuel(fuel, false)
    required_fuel_for_fuel(morefuel, morefuel + acc)
  end

  @doc """
  Reads module masses and calculates total required fuel.
  """
  @spec fuel_sum(String.t(), boolean()) :: integer()
  def fuel_sum(filename, account_fuel_for_fuel \\ false) do
    modules =
      Path.expand(filename)
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Stream.map(&String.to_integer(&1))

    Task.async_stream(modules, fn mass -> required_fuel(mass, account_fuel_for_fuel) end)
    |> Enum.reduce(0, fn {:ok, num}, acc -> num + acc end)
  end
end
