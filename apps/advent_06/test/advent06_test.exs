defmodule Advent06Test do
  use ExUnit.Case
  doctest Advent06

  test "passes the first sub-challenge" do
    assert Advent06.sum_of_stored_orbits("priv/input.txt") == 315_757
  end

  test "passes the second sub-challenge" do
    assert Advent06.orbit_jumps_from_map("priv/input.txt", "YOU", "SAN") == 481
  end
end
