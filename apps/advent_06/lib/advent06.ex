defmodule Advent06 do
  @moduledoc """
  Source code for Advent of Code puzzle (Day 6).
  """

  @doc """
  Counts the total number of orbits.

  ## Examples

      iex> Advent06.sum_of_orbits(["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L"])
      42

      iex> Advent06.sum_of_orbits(["K)L", "J)K", "E)J", "D)I", "G)H", "B)G", "E)F", "D)E", "C)D", "B)C", "COM)B"])
      42
  """
  @spec sum_of_orbits(list(String.t())) :: integer()
  def sum_of_orbits(orbits) do
    process_orbits([], orbits)
    |> Enum.reduce(0, fn {_node, depth, _children}, acc -> depth + acc end)
  end

  @spec process_orbits(list({String.t(), integer(), list(String.t())}), list(String.t())) ::
          list({String.t(), integer(), list(String.t())})
  defp process_orbits(acc, []), do: acc

  defp process_orbits(acc, [orbit | moreorbits]) do
    [parent, child] = String.split(orbit, ")")

    case {List.keyfind(acc, parent, 0), List.keyfind(acc, child, 0)} do
      {nil, nil} ->
        [{parent, 0, [child]}, {child, 1, []} | acc]

      {nil, {child, 0, childrenofchild}} ->
        [
          {parent, 0, [child]}
          | increase_level(
              childrenofchild,
              2,
              List.keyreplace(acc, child, 0, {child, 1, childrenofchild})
            )
        ]

      {{parent, parent_level, children}, nil} ->
        [
          {child, parent_level + 1, []}
          | List.keyreplace(acc, parent, 0, {parent, parent_level, [child | children]})
        ]

      {{parent, parent_level, children}, {child, _child_level, childrenofchild}} ->
        increase_level(
          childrenofchild,
          parent_level + 2,
          List.keyreplace(acc, child, 0, {child, parent_level + 1, childrenofchild})
        )
        |> List.keyreplace(parent, 0, {parent, parent_level, [child | children]})
    end
    |> process_orbits(moreorbits)
  end

  @spec increase_level(
          list(String.t()),
          integer(),
          list({String.t(), integer(), list(String.t())})
        ) :: list({String.t(), integer(), list(String.t())})
  defp increase_level([], _level, acc), do: acc

  defp increase_level([node | rest], level, acc) do
    {node, _level, children} = List.keyfind(acc, node, 0)
    new_acc = increase_level(rest, level, List.keyreplace(acc, node, 0, {node, level, children}))
    increase_level(children, level + 1, new_acc)
  end

  @doc """
  Counts the total number of orbits, given a file that stores them.

  """
  @spec sum_of_stored_orbits(String.t()) :: integer()
  def sum_of_stored_orbits(filename) do
    load(filename) |> sum_of_orbits
  end

  @doc """
  Counts the total number of orbits jumbs needed to reach one space object from another.

  ## Examples

      # iex> Advent06.orbit_jumps(["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L", "K)YOU", "I)SAN"], "YOU", "SAN")
      # 4

      # iex> Advent06.orbit_jumps(["K)YOU", "I)SAN", "K)L", "J)K", "E)J", "D)I", "G)H", "B)G", "E)F", "D)E", "C)D", "B)C", "COM)B"], "YOU", "SAN")
      # 4

  """
  @spec orbit_jumps(list(String.t()), String.t(), String.t()) :: integer()
  def orbit_jumps(orbits, from, to) do
    orbit_map = process_orbits([], orbits)
    {from, depth_f, _children} = List.keyfind(orbit_map, from, 0)
    {to, depth_t, _children} = List.keyfind(orbit_map, to, 0)
    {_parent, depth_p} = common_parent({from, depth_f}, {to, depth_t}, orbit_map)
    depth_f - depth_p - 1 + (depth_t - depth_p - 1)
  end

  @spec common_parent(
          {String.t(), integer()},
          {String.t(), integer()},
          list({String.t(), integer(), list(String.t())})
        ) :: {String.t(), integer()}
  defp common_parent({from, depth_f}, {to, depth_t}, orbit_map) when depth_f > depth_t do
    # just a slight optimization to inspect less possible parents
    common_parent(to, from, orbit_map)
  end

  defp common_parent({from, _depth_f}, {to, _depth_t}, orbit_map) do
    common_parent(from, to, orbit_map)
  end

  defp common_parent(a, b, orbit_map) do
    [{parent_a, depth_a, children}] =
      Enum.filter(orbit_map, fn {_, _, list} -> Enum.member?(list, a) end)

    cond do
      Enum.member?(children, b) or is_descendent(b, children, orbit_map) -> {parent_a, depth_a}
      true -> common_parent(parent_a, b, orbit_map)
    end
  end

  @spec is_descendent(
          String.t(),
          list(String.t()),
          list({String.t(), integer(), list(String.t())})
        ) :: boolean()
  defp is_descendent(_, [], _), do: false

  defp is_descendent(b, [child | rest], orbit_map) do
    {_, _, children} = List.keyfind(orbit_map, child, 0)
    Enum.member?(children, b) or is_descendent(b, rest ++ children, orbit_map)
  end

  @doc """
  Counts the total number of orbits jumbs needed to reach one space object from another, given a file that stores the stellar map.

  """
  @spec orbit_jumps_from_map(String.t(), String.t(), String.t()) :: integer()
  def orbit_jumps_from_map(filename, from, to) do
    load(filename) |> orbit_jumps(from, to)
  end

  @spec load(String.t()) :: list(String.t())
  defp load(filename) do
    Path.expand(filename)
    |> Path.absname()
    |> File.read!()
    |> String.trim("\n")
    |> String.split("\n")
    |> Enum.to_list()
  end
end
