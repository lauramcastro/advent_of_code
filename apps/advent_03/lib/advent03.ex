defmodule Advent03 do
  @moduledoc """
  Source code for Advent of Code puzzle (Day 3).
  """

  @doc """
  Calculates full wire path.

  ## Examples

      iex> Advent03.full_wire_path([{'R',8},{'U',5},{'L',5},{'D',3}])
      [{{1,0},1},{{2,0},2},{{3,0},3},{{4,0},4},{{5,0},5},{{6,0},6},{{7,0},7},{{8,0},8},
       {{8,1},9},{{8,2},10},{{8,3},11},{{8,4},12},{{8,5},13},
       {{7,5},14},{{6,5},15},{{5,5},16},{{4,5},17},{{3,5},18},
       {{3,4},19},{{3,3},20},{{3,2},21}]

  """
  @spec full_wire_path(list({char(), integer()})) :: list({{integer(), integer()}, integer()})
  def full_wire_path(path_moves) do
    process_path(path_moves, 0, 0, 0, [])
  end

  @spec process_path(
          list({char(), integer()}),
          integer(),
          integer(),
          integer(),
          list({{integer(), integer()}, integer()})
        ) :: list({{integer(), integer()}, integer()})
  defp process_path([], _current_x, _current_y, _current_steps, acc) do
    acc
  end

  defp process_path([move | rest], current_x, current_y, current_steps, acc) do
    {new_x, new_y, path, updated_steps} = process_move(move, current_x, current_y, current_steps)
    process_path(rest, new_x, new_y, updated_steps, acc ++ path)
  end

  @spec process_move({charlist(), integer()}, integer(), integer(), integer()) ::
          {integer(), integer(), list({{integer(), integer()}, integer()}), integer()}
  defp process_move({'U', units}, x, y, steps) do
    path_steps =
      Enum.to_list((y + 1)..(y + units))
      |> Enum.map(fn n -> {{x, n}, steps + n - y} end)

    {x, y + units, path_steps, steps + units}
  end

  defp process_move({'D', units}, x, y, steps) do
    path_steps =
      Enum.to_list((y - 1)..(y - units))
      |> Enum.map(fn n -> {{x, n}, steps + (n - y) * -1} end)

    {x, y - units, path_steps, steps + units}
  end

  defp process_move({'R', units}, x, y, steps) do
    path_steps =
      Enum.to_list((x + 1)..(x + units))
      |> Enum.map(fn n -> {{n, y}, steps + n - x} end)

    {x + units, y, path_steps, steps + units}
  end

  defp process_move({'L', units}, x, y, steps) do
    path_steps =
      Enum.to_list((x - 1)..(x - units))
      |> Enum.map(fn n -> {{n, y}, steps + (n - x) * -1} end)

    {x - units, y, path_steps, steps + units}
  end

  @doc """
  Calculates two wire path crossings.

  ## Examples

      iex> Advent03.wire_crossings([{'R',8},{'U',5},{'L',5},{'D',3}],[{'U',7},{'R',6},{'D',4},{'L',4}])
      [{{6,5},30},{{3,3},40}]

  """
  @spec wire_crossings(
          list({char(), integer()}),
          list({char(), integer()})
        ) :: list({{integer(), integer()}, integer()})
  def wire_crossings(path_a, path_b) do
    [full_path_a, full_path_b] =
      Task.async_stream([path_a, path_b], fn path -> full_wire_path(path) end)
      |> Enum.reduce([], fn {:ok, x}, acc -> [Enum.to_list(x) | acc] end)

    for {position, steps} <- full_path_a,
        List.keymember?(full_path_b, position, 0) do
      {position, moresteps} = List.keyfind(full_path_b, position, 0)
      {position, steps + moresteps}
    end
  end

  @doc """
  Calculates distance to closest-to-central-port wire path crossings.

  ## Examples

      iex> Advent03.distance([{'R',8},{'U',5},{'L',5},{'D',3}],[{'U',7},{'R',6},{'D',4},{'L',4}])
      6

      iex> Advent03.distance([{'R',75},{'D',30},{'R',83},{'U',83},{'L',12},{'D',49},{'R',71},{'U',7},{'L',72}],[{'U',62},{'R',66},{'U',55},{'R',34},{'D',71},{'R',55},{'D',58},{'R',83}])
      159

      iex> Advent03.distance([{'R',98},{'U',47},{'R',26},{'D',63},{'R',33},{'U',87},{'L',62},{'D',20},{'R',33},{'U',53},{'R',51}],[{'U',98},{'R',91},{'D',20},{'R',16},{'D',67},{'R',40},{'U',7},{'R',15},{'U',6},{'R',7}])
      135

  """
  @spec distance(
          list({char(), integer()}),
          list({char(), integer()})
        ) :: integer()
  def distance(path_a, path_b) do
    distance(path_a, path_b, :manhattan)
  end

  @spec distance(list({char(), integer()}), list({char(), integer()}), atom()) :: integer()
  defp distance(path_a, path_b, measure) do
    wire_crossings(path_a, path_b)
    |> Task.async_stream(fn x -> measure(x, measure) end)
    |> Enum.map(fn {:ok, distance} -> distance end)
    |> Enum.sort()
    |> List.first()
  end

  @spec measure({{integer(), integer()}, integer()}, atom()) :: integer()
  defp measure({cross_point, _steps}, :manhattan), do: manhattan_distance(cross_point)
  defp measure({_cross_point, steps}, :steps), do: steps

  @spec manhattan_distance({integer(), integer()}) :: integer()
  defp manhattan_distance({x, y}), do: abs(x) + abs(y)

  @doc """
  Calculates distance to closest-to-central-port wire path crossings, given in a file.

  """
  @spec distance(String.t()) :: integer()
  def distance(filename) do
    [path_a, path_b] = load_paths_from_file(filename)
    distance(path_a, path_b, :manhattan)
  end

  @spec load_paths_from_file(String.t()) :: list(list({char(), integer()}))
  defp load_paths_from_file(filename) do
    string_paths =
      Path.expand(filename)
      |> Path.absname()
      |> File.read!()
      |> String.trim("\n")
      |> String.split("\n")

    paths = Task.async_stream(string_paths, fn path -> process_path(path) end)
    [{:ok, path_a}, {:ok, path_b}] = Enum.to_list(paths)
    [path_a, path_b]
  end

  @spec process_path(String.t()) :: list(integer())
  defp process_path(string) do
    String.split(string, ",")
    |> Enum.map(&process_move(&1))
  end

  @spec process_move(String.t()) :: {charlist(), integer()}
  defp process_move(move) do
    {String.slice(move, 0..0) |> String.to_charlist(),
     String.slice(move, 1..3) |> String.to_integer()}
  end

  @doc """
  Calculates distance (in steps) to closest-to-central-port wire path crossings, given in a file.

  """
  @spec steps(String.t()) :: integer()
  def steps(filename) do
    [path_a, path_b] = load_paths_from_file(filename)
    distance(path_a, path_b, :steps)
  end
end
