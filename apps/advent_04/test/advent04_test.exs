defmodule Advent04Test do
  use ExUnit.Case
  doctest Advent04

  test "passes the second sub-challenge" do
    assert Advent04.number_of_matches(245_182, 790_572) == 710
  end
end
