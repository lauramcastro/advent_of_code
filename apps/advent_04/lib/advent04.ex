defmodule Advent04 do
  @moduledoc """
  Source code for Advent of Code puzzle (Day 4).
  """

  # @range_starts 245182
  # @range_ends 790572

  @doc """
  Determines whether a password matches certain list of criteria.

  Namely:
  - it is a six digit number
  - *exactly* two adjancent digits are the same
  - digits never decrease

  ## Examples

      iex> Advent04.matches_criteria(122345)
      true

      iex> Advent04.matches_criteria(111111)
      false

      iex> Advent04.matches_criteria(223450)
      false

      iex> Advent04.matches_criteria(123789)
      false

      iex> Advent04.matches_criteria(112233)
      true

      iex> Advent04.matches_criteria(123444)
      false

      iex> Advent04.matches_criteria(111122)
      true

  """
  @spec matches_criteria(integer()) :: boolean()
  def matches_criteria(password) do
    Enum.all?(
      [
        &has_six_digits/1,
        &two_adjacent/1,
        &never_decreases/1
      ],
      fn f -> apply(f, [password]) end
    )
  end

  @spec has_six_digits(integer()) :: boolean()
  defp has_six_digits(n) do
    length(Integer.digits(n)) == 6
  end

  # @spec in_range(integer()) :: boolean()
  # defp in_range(n) when n > @range_starts and n < @range_ends, do: true
  # defp in_range(_n), do: false

  @spec two_adjacent(integer()) :: boolean()
  defp two_adjacent(n), do: two_adjacent_aux(Integer.digits(n), 0)

  @spec two_adjacent_aux(list(char()), integer()) :: boolean()
  defp two_adjacent_aux([a, a], 0), do: true
  defp two_adjacent_aux([a, a], _n), do: false
  defp two_adjacent_aux([_a, _b], n), do: n == 2
  defp two_adjacent_aux([a, a | rest], 0), do: two_adjacent_aux([a | rest], 2)
  defp two_adjacent_aux([a, a | rest], n), do: false or two_adjacent_aux([a | rest], n + 1)
  defp two_adjacent_aux([_a, b | rest], 0), do: two_adjacent_aux([b | rest], 0)
  defp two_adjacent_aux([_a, _b | _rest], 2), do: true
  defp two_adjacent_aux([_a, b | rest], _n), do: false or two_adjacent_aux([b | rest], 0)

  @spec never_decreases(integer()) :: boolean()
  defp never_decreases(n), do: never_decreases_aux(Integer.digits(n))

  @spec never_decreases_aux(list(char())) :: boolean()
  defp never_decreases_aux([a, b]), do: a <= b
  defp never_decreases_aux([a, b | rest]) when a <= b, do: never_decreases_aux([b | rest])
  defp never_decreases_aux(_), do: false

  @doc """
  Determines how many passwords in a given range match certain list of criteria.

  """
  @spec number_of_matches(integer, integer) :: integer
  def number_of_matches(range_start, range_end) do
    Enum.to_list(range_start..range_end)
    |> Task.async_stream(fn p -> matches_criteria(p) end)
    |> Enum.reduce(0, fn
      {:ok, true}, acc -> acc + 1
      {:ok, false}, acc -> acc
    end)
  end
end
