defmodule Advent08 do
  @moduledoc """
  Source code for Advent of Code puzzle (Day 8).
  """

  @doc """
  Decodes image layers from a stream of digits, given its dimensions.

  ## Examples

      iex> Advent08.to_layers("123456789012", 3, 2)
      [[1,2,3,4,5,6],[7,8,9,0,1,2]]

  """
  @spec to_layers(String.t(), integer(), integer()) :: list(list(integer()))
  def to_layers(digit_stream, width, height) do
    to_layers_aux(digit_stream, width * height, [])
  end

  @spec to_layers_aux(String.t(), integer(), list(list(integer()))) :: list(list(integer()))
  defp to_layers_aux("", _, acc), do: Enum.reverse(acc)

  defp to_layers_aux(stream, len, acc) do
    {layer, rest} = String.split_at(stream, len)
    layer_as_digits = for x <- String.codepoints(layer), do: String.to_integer(x)
    to_layers_aux(rest, len, [layer_as_digits | acc])
  end

  @doc """
  Decodes image layers from a stream of digits, given its dimensions and a filename where the image is stored.
  """
  @spec from_file_to_layers(String.t(), integer(), integer()) :: integer()
  def from_file_to_layers(filename, width, height) do
    layers = load(filename) |> to_layers(width, height)

    {_fewest_zeroes, layer} =
      Task.async_stream(layers, fn layer -> {Enum.count(layer, &equals(&1, 0)), layer} end)
      |> Enum.map(fn {:ok, reply} -> reply end)
      |> Enum.sort()
      |> List.first()

    Enum.count(layer, &equals(&1, 1)) * Enum.count(layer, &equals(&1, 2))
  end

  @spec equals(integer(), integer()) :: boolean()
  defp equals(a, a), do: true
  defp equals(_a, _b), do: false

  @spec load(String.t()) :: String.t()
  defp load(filename) do
    Path.expand(filename)
    |> Path.absname()
    |> File.read!()
    |> String.trim("\n")
  end

  @doc """
  Collapses image layers into a single one based on visibility.

  ## Examples

      iex> Advent08.collapse("0222112222120000", 2, 2)
      [[1,0],[0,1]]

  """
  @spec collapse(String.t(), integer(), integer()) :: list(list(integer()))
  def collapse(digit_stream, width, height) do
    layers = to_layers(digit_stream, width, height)
    agents = for layer <- layers, do: Agent.start_link(fn -> layer end)
    collapse_aux(agents, width * height, []) |> slice(width, height, [])
  end

  @spec collapse_aux(list({:ok, pid()}), integer(), list(integer())) :: list(integer())
  defp collapse_aux(agents, 0, acc) do
    for {:ok, agent} <- agents, do: :ok = Agent.stop(agent)
    acc
  end

  defp collapse_aux(agents, n, acc) do
    values =
      for {:ok, agent} <- agents,
          do: Agent.get_and_update(agent, fn state -> {hd(state), tl(state)} end)

    collapse_aux(agents, n - 1, [collapse_values(values, 2) | acc])
  end

  @spec collapse_values(list(integer()), integer()) :: integer()
  # 0 is black, 1 is white, 2 is transparent
  defp collapse_values([], acc), do: acc
  # we find black, we do not look underneath
  defp collapse_values([0 | _rest], _acc), do: 0
  # we find white, we do not look underneath
  defp collapse_values([1 | _rest], _acc), do: 1
  # we find transparent, we recursively look underneath
  defp collapse_values([2 | rest], acc), do: collapse_values(rest, acc)

  @spec slice(list(integer()), integer(), integer(), list(list(integer()))) ::
          list(list(integer()))
  defp slice(_list, _width, 0, acc), do: acc

  defp slice(list, width, height, acc) do
    {slice, rest} = Enum.split(list, width)
    slice(rest, width, height - 1, [slice | acc])
  end

  @doc """
  Decodes image, collapsing its layers, from a stream of digits, given its dimensions and a filename where the image is stored.
  """
  @spec render_file(String.t(), integer(), integer()) :: integer()
  def render_file(filename, width, height) do
    layers = load(filename)

    list = collapse(layers, width, height)
    {:ok, file} = File.open("priv/output.txt", [:write])

    for l <- list do
      s =
        for x <- Enum.reverse(l) do
          Integer.to_string(x)
        end

      readable = List.to_string(s) |> String.replace("0", " ") |> String.replace("1", "*")

      IO.binwrite(file, readable <> "\n")
    end

    File.close(file)

    length(list)
  end
end
