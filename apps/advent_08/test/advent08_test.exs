defmodule Advent08Test do
  use ExUnit.Case
  doctest Advent08

  test "passes the first sub-challenge" do
    assert Advent08.from_file_to_layers("priv/input.txt", 25, 6) == 1330
  end

  test "passes the second sub-challenge" do
    assert Advent08.render_file("priv/input.txt", 25, 6) == 6
  end
end
